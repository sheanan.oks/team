""" Simulate data from [ Dose-related effects of red wine and alcohol on hemodynamics, sympathetic nerve activity, and arterial diameter
Jonas Spaak  1 , Anthony C Merlocco, George J Soleas, George Tomlinson, Beverley L Morris, Peter Picton, Catherine F Notarius, Christopher T Chan, John S Floras
](https://pubmed.ncbi.nlm.nih.gov/18055508/)
