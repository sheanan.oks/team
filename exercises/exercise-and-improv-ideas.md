# Exercise Ideas

- [x] FAQ example for ch03
- [x] Sparse TF-IDF matrices -> sparse data frames
- [ ] big data chunking for simple statistics aggregation
- [ ] sampling (train/test split) with big data chunking
- [ ] SGD .partial fit for chunked data
- [ ] active learning: high conf = move train->test, low conf = move test->train
- [ ] https://github.com/tangibleai/sempre
