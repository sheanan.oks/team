# Mastery self-evaluation

You've completed the internship!
Your last exercise is to review your progress and revisit that checklist you created at the start of the internship.
This will give you one last chance to practice your `git` and *GitLab* skills.

You mission is to complete this checklist, about your skills checklist... how meta.
Try not to click on the links (answers) shown in the [Exercise Checklist](#exercise-checklist) checklist below.
That way you'll know what to do at your first job where they use gitlab to manage their software.
You can use [The Duck](http://duckduckgo.com) or the [GitLab's search bar](https://gitlab.com/search).
To practice active learning, try guessing the url before using [The Duck](http://duckduckgo.com) or the GitLab search feature.
You can even try your guess by typing it into the navigation bar of your browser and taking advantage of auto-complete (if you have a helpful (prosocial) browser like Firefox.
You're a superstar active learner if you can find these pages without clicking on the links in the checklist below.
And you're a commandline superstar if you do the file creation and editing using `git` and a `text editor` on your laptop.

## Skills checklist retrospective

TLDR; Find the skills checklist issue that you created at the beginning of the internship or the updated `blank.md` checklist; add it to the team repo in the interns/skills-checklist directory using your first name as the file name; fill it out.

- [ ] What is the URL for the Tangible AI's organization on GitLab? [answer](https://gitlab.com/tangibleai/)
- [ ] What is the URL for the Tangible AI's `team` repository on GitLab? [answer](https://gitlab.com/tangibleai/team/)
- [ ] What is the URL for the list of issues & features (tickets) within Tangible AI's `team` repository? [answer](https://gitlab.com/tangibleai/team/-/issues)
- [ ] Find your skills checklist issue within Tangible AI's teams repository on gitlab: [here's mine](https://gitlab.com/tangibleai/team/-/issues/1)
- [ ] Copy the raw markdown text from your issue (use the Edit button if you like).
- [ ] Create a new `your_name.md` file in the `interns/skills-checklist` directory (use the '+' sign next to the file path).
- [ ] Paste your skills checklist into the new markdown file you created.
- [ ] Save (commit) it to the repository.
- [ ] Edit the skills checklist to check off any skills you've added or improved during the internship.
- [ ] Save (commit and push) your updated checklist.
- [ ] Post a link to your skills checklist on the #interns slack channel.

Thank you!
I'll write a script to parse your checkliss and help us improve the internship program.
I've also updated the [blank skills checklist](https://gitlab.com/tangibleai/team/-/blob/master/interns/skills-checklist/blank.md)
And you're always welcome to come back here in the future to review and update your learning progress.
This file is yours, feel free to add notes or additional things you'd like to learn in the future.
And you can do a diff or merge with the [blank skills checklist](https://gitlab.com/tangibleai/team/-/blob/master/interns/skills-checklist/blank.md) to add addition TODOs to your life.

## Bonus

- [ ] Announce your internship success on on social media or your professional profile (LinkedIn ;-( ) with a link to your skills checklist!
- [ ] Share your project final report on social media or your professional profile with a link to your skills checklist!
- [ ] Post a link on social media or your professional profile (LinkedIn?) to your final report!
- [ ] Find the link to the diff for the changes to your skills checklist and post the diff to #interns
