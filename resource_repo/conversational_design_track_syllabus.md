## Week 1

Learn about Tangible AI and what we do: https://us02web.zoom.us/rec/share/P0FQIaaoUoaTfpqWTAJbHl6qq6vqZwViXwibKQjL1NWC2DGWqR0L2S3myHWd_JOr.RyUdZgIc4lbdVuRZ?startTime=1580437251000

Hear a beginner lecture about conversational design (Maria's): https://drive.google.com/drive/folders/16mRieOLhUE4wMVPeC0ry-TabYXtZp1cz?usp=sharing

Hear a beginner lecture about conversational design (Effective Copywriting for Chatbots): https://www.youtube.com/watch?v=49G58PQWO7w 

Task: Play with at least 2-3 bots from https://tangibleai.com/social-impact-chatbot-database/. Answer the following questions for yourself: 
* Which chatbot did you like the best?
* What is one thing you would improve about each chatbot?
* 

## Week 2 

Basics of UX design:

Think about the functionality of your chatbot. What value it's going to be offering to users? What are going to be the "functions" it's going to perform.

Choose a "sandbox project" 
* "Know before you go"
* "Tai Tanzania" chatbot 


== Week 3: 
Create the outline of your chatbot. What are the main bulding blocks? 
