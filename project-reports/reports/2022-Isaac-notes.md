# Isaac's Notes

I will be keeping track of my progress throughout the course of the internship here. This report will contain learning notes, documentation of projects etc.

## Notes from Week 1 Meeting with Hobson

1. Determine projects of interest for the duration of the internship. I suggested something to do with Social Media. Hobson suggested to look into Open Source Protocols and Social Platforms like:

    * mastodon.social

    * Tusky

    * ActivityPub

    Hobson also suggested to skim through the list of projects.

2. Setup my checklist for skills to develop during the course of the internship.